#!/bin/sh
curl -o list.xml https://raw.github.com/zapret-info/z-i/master/dump.csv
# Get IP addresses from list
./getips.sh
# Get hostnames from list
./gethosts.sh
# Resolve A record from hostnames
./resolvehosts.sh
# Generate .pac with IP addresses
./genpac-ip.sh
# Generate .pac with IP addresses and hostnames
./genpac-hostname.sh
# Generate OpenVPN .ccd
./genopenvpn.sh
# Generate dnsmasq aliases
./gendnsmasq.sh
# Add IP addresses to iptables
./setiptables.sh
# Restart dnsmasq
service dnsmasq restart
# Restart polipo
ulimit -n 8192
#service polipo restart
# Save iptables
service iptables-persistent save
