#!/bin/bash
source ./config.sh

# .pac header
echo "// ProstoVPN.AntiZapret PAC-host File
// Generated on $(date)

function FindProxyForURL(url, host) {
  blockedips = [ " > proxy-host.pac


while read line
do
	echo "        \"$line\"," >> proxy-host.pac
done < iplist.txt

echo "      ];

  blockedhosts = [ " >> proxy-host.pac

while read line
do
        echo "        \"$line\"," >> proxy-host.pac
done < hostlist.txt

echo "      ];
" >> proxy-host.pac

echo "  if (blockedips.indexOf(dnsResolve(host)) != -1) {
    return \"PROXY ${PACPROXYHOST}; DIRECT\";
  }

  for (var i = 0; i < blockedhosts.length; i++) {
    if (dnsDomainIs(host, "." + blockedhosts[i]) || host == blockedhosts[i]) {
      return \"PROXY ${PACPROXYHOST}; DIRECT\";
    }
  }

  return \"DIRECT\";
}" >> proxy-host.pac

cp ./proxy-host.pac /usr/share/nginx/html/antizapret/
