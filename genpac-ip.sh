#!/bin/bash
source ./config.sh

# .pac header
echo "// ProstoVPN.AntiZapret PAC-ip File
// Generated on $(date)

function FindProxyForURL(url, host) {
  blockedips = [ " > proxy.pac

while read line
do
	echo "        \"$line\"," >> proxy.pac
done < iplist.txt

echo "      ];
" >> proxy.pac

echo "  if (blockedips.indexOf(dnsResolve(host)) != -1) {
    return \"PROXY ${PACPROXYHOST}; DIRECT\";
  }

  return \"DIRECT\";
}" >> proxy.pac

cp ./proxy.pac /usr/share/nginx/html/antizapret/
