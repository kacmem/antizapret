#!/bin/bash

# Add IP addresses to iptables 'zapret' table
echo "Flushing iptables rules"
iptables -F zapret

echo "Reading iplist.txt"
while read line
do
	iptables -A zapret -d "$line" -j ACCEPT
done < iplist.txt

echo "Done!"

