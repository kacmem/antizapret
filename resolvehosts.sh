#!/bin/sh

# Resolve A records from hostnames
rm ./resolved-hostnames/*
while read line
do
	echo "Resolving $line"
	dig +short "$line" > ./resolved-hostnames/"$line"
done < hostlist.txt

cp iplist.txt resolved-hostnames/
cat resolved-hostnames/* | egrep '([0-9]{1,3}\.){3}[0-9]' | sort | uniq | grep -v -f ignoreips.txt > iplist.txt
echo -n "Total IPs after resolving: "
cat iplist.txt | wc -l
